(define (fact n)
  (cond ((= n 1) 1)
        (else (* n (fact (sub1 n))))))

(define (sum-digits n)
  (apply + (map (lambda (c)
                  (string->number (string c)))
                (string->list
                 (number->string n)))))

(sum-digits (fact 100))