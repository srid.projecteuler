(require "streams.ss")

(define (nth-prime n)
  (stream-ref primes (sub1 n)))

(nth-prime 10001)