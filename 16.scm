(define (power a n)
  (if (= n 0)
      1
      (* a (power a (sub1 n)))))

(define (sum-digits n)
  (apply + (map (lambda (c)
                  (string->number (string c)))
                (string->list
                 (number->string n)))))

(sum-digits (power 2 1000))