(require "streams.ss")

(define N 317584931803)

(define (largest-prime-factor n primes)
  (let ((p (stream-car primes)))
    (cond ((= p n) p)
          (else 
           (if (= (modulo n p) 0)
               (largest-prime-factor (/ n p) primes)
               (largest-prime-factor n (stream-cdr primes)))))))

(largest-prime-factor N primes)