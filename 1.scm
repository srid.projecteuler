(require (only (lib "1.ss" "srfi") filter))

(define (numbers a b)
  (cond ((> a b) null)
        (else (cons a (numbers (add1 a) b)))))

(apply + (filter (lambda (x)
                   (or (= (modulo x 3) 0)
                       (= (modulo x 5) 0)))
                 (numbers 1 999)))