(require (planet "memoize.ss" ("dherman" "memoize.plt" 2 1)))

(define (divides? n k)
  (= 0
     (modulo n k)))

(define/memo (sum-of-proper-divisors n)
  (define (other k)
    (if (or (= k 1)
            (= (* k k) n))
        0
        (/ n k)))
  
  (let ((ceil (floor (sqrt n))))
    (let loop ((try 1)
               (total 0))
      (if (>= try ceil)
          total
          (if (divides? n try)
              (loop (add1 try) (+ total try (other try)))
              (loop (add1 try) total))))))

(let loop ((n 1)
           (S 0))
  (if (= n 10000)
      S
      (let* ((sopd (sum-of-proper-divisors n))
             (sopd* (sum-of-proper-divisors sopd)))
        (if (and (= n sopd*) (< n sopd))
            (begin
              (display (format "~a:~a" n sopd))(newline)
              (loop (add1 n) (+ S n sopd)))
            (loop (add1 n) S)))))