(require (only (lib "1.ss" "srfi") fold))

(define (numbers a b)
  (cond ((> a b) null)
        (else (cons a (numbers (add1 a) b)))))

(define (gcd a b)
  (cond ((= a 0)
         b)
        ((= b 0)
         a)
        (else
         (gcd b (modulo a b)))))

(fold gcd 1 (numbers 2 10))