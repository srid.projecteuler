(require (planet "memoize.ss" ("dherman" "memoize.plt" 2 1)))

(define (number-of-routes n)
  (define/memo (find x y)
    (if (and (= x n) (= y n))
        1
        (+ (if (= x n) 0 (find (add1 x) y))
           (if (= y n) 0 (find x (add1 y))))))
  
  (find 0 0))

(number-of-routes 20)