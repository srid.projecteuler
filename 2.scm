(require (only (lib "1.ss" "srfi") filter)
         "streams.ss")

(define (sum-even-fibonacci limit fibs sum)
  (cond ((> (stream-car fibs) limit) 
         sum)
        ((= (modulo (stream-car fibs) 2) 0)
         (sum-even-fibonacci 
          limit (stream-cdr fibs) (+ sum (stream-car fibs))))
        (else
         (sum-even-fibonacci
          limit (stream-cdr fibs) sum))))

(sum-even-fibonacci 1000000 fibonacci 0)