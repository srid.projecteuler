(require (only (lib "list.ss") sort))

(define (numbers a b)
  (cond ((> a b) null)
        (else (cons a (numbers (add1 a) b)))))

(define (cross l1 l2)
  (if (null? l1)
      '()
      (append (map (lambda (x)
                     (cons (car l1) x))
                   l2)
              (cross (cdr l1) l2))))

(define 3-digit-numbers (numbers 111 999))

(define sorted-pairs (reverse (sort (cross 3-digit-numbers
                                           3-digit-numbers)
                                    (lambda (x y)
                                      (< (* (car x) (cdr x))
                                         (* (car y) (cdr y)))))))

(define (palindrome? s)
  (equal? s
          (list->string (reverse (string->list s)))))

(let loop ((pairs sorted-pairs))
  (if (null? pairs)
      #f
      (let* ((pair (car pairs))
             (num  (number->string (* (caar pairs)
                                      (cdar pairs)))))
        (if (palindrome? num)
            num
            (loop (cdr pairs))))))

