(define (numbers a b)
  (cond ((> a b) null)
        (else (cons a (numbers (add1 a) b)))))

(define one-hundred (numbers 1 100))

(define (square n) (* n n))

(- (apply + (map square one-hundred))
   (square (apply + one-hundred)))